﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace CSharpManualResetEvent
{
    public partial class Form1 : Form
    {
        // マニュアルリセットイベントを作成します。
        private ManualResetEvent mre = new ManualResetEvent(false);

        private int count = 0;

        public Form1()
        {
            InitializeComponent();

            // 非同期処理を開始します。
            Task.Run(() =>
            {
                while (true)
                {
                    // シグナル状態になるのを待ちます。
                    mre.WaitOne();

                    // ラベルの表示を更新します。
                    this.Invoke(new MethodInvoker(delegate
                    {
                        lblMessage.Text = "Button press count = " + ++count;
                    }));

                    // シグナル状態をリセットします。
                    mre.Reset();
                }
            });
        }

        private void btnCountUp_Click(object sender, EventArgs e)
        {
            // シグナル状態にします。
            mre.Set();
        }
    }
}
